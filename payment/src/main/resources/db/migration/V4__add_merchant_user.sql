INSERT INTO public.application_user(
    user_type, id, email, password, name)
VALUES ('MERCHANT', '28cb74af-621a-4650-a5fe-0bee2470a592', 'merchant@mail.com', '$2a$10$VLFCHDu72pt0Ha08UMHne.WsxJtGqyuYqzsR4DZ6m3jw8513TVOwq', 'Merchant Merchant');


INSERT INTO public.merchant(
    description, status, total_transaction_sum, id)
VALUES ('test merchant description',  'ACTIVE', 0, '28cb74af-621a-4650-a5fe-0bee2470a592');