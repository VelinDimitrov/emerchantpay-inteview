CREATE TABLE public.application_user (
                                         user_type varchar(31) NOT NULL,
                                         id uuid NOT NULL,
                                         email varchar(255) NOT NULL,
                                         "password" varchar(255) NOT NULL,
                                         "name" varchar(255) NULL,
                                         CONSTRAINT application_user_pkey PRIMARY KEY (id),
                                         CONSTRAINT uk_cb61p28hanadv7k0nx1ec0n5l UNIQUE (email)
);

CREATE TABLE public.admin_user (
                                   id uuid NOT NULL,
                                   CONSTRAINT admin_user_pkey PRIMARY KEY (id)
);


ALTER TABLE public.admin_user ADD CONSTRAINT fktn7t8obu7c4ai7i62h0jv98hm FOREIGN KEY (id) REFERENCES application_user(id);


CREATE TABLE public.merchant (
                                 description varchar(255) NULL,
                                 status varchar(255) NULL,
                                 total_transaction_sum numeric(19,2) NULL,
                                 id uuid NOT NULL,
                                 CONSTRAINT merchant_pkey PRIMARY KEY (id)
);

ALTER TABLE public.merchant ADD CONSTRAINT fkrg90liddgkw91efl3ubjro94b FOREIGN KEY (id) REFERENCES application_user(id);

CREATE TABLE public."transaction" (
                                      transaction_type varchar(31) NOT NULL,
                                      id uuid NOT NULL,
                                      customer_email varchar(255) NULL,
                                      customer_phone varchar(255) NULL,
                                      status varchar(255) NULL,
                                      transaction_date timestamp NULL,
                                      amount numeric(19,2) NULL,
                                      merchant_id uuid NULL,
                                      reference_id uuid NULL,
                                      CONSTRAINT transaction_pkey PRIMARY KEY (id)
);

ALTER TABLE public."transaction" ADD CONSTRAINT fkcwlugq1eich0rct1w83pewkqc FOREIGN KEY (merchant_id) REFERENCES merchant(id);
ALTER TABLE public."transaction" ADD CONSTRAINT fkrhyk34gouivxi0s43mcji7ew5 FOREIGN KEY (reference_id) REFERENCES transaction(id);