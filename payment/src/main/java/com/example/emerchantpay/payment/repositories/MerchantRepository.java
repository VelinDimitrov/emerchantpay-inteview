package com.example.emerchantpay.payment.repositories;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.entities.MerchantStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MerchantRepository extends JpaRepository<Merchant, UUID> {
    Merchant findByIdAndStatus(UUID id, MerchantStatus status);

    Merchant findByEmail(String email);
}
