package com.example.emerchantpay.payment.pojos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ValidationErrorResponse {
    private String fieldName;
    private String errorMessage;
}
