package com.example.emerchantpay.payment.advices;

import com.example.emerchantpay.payment.pojos.ErrorResponse;
import com.example.emerchantpay.payment.exceptions.TransactionsForMerchantFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class TransactionsForMerchantFoundExceptionHandler {
    @ExceptionHandler(value = TransactionsForMerchantFoundException.class)
    public ResponseEntity<ErrorResponse> exception(TransactionsForMerchantFoundException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
