package com.example.emerchantpay.payment.services.interfaces;

import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import com.example.emerchantpay.payment.pojos.UserDto;

import java.util.List;
import java.util.UUID;

public interface TransactionService {
    List<TransactionResponse> getTransactionsForMerchant(UUID merchantId, UserDto userDto);

    TransactionResponse createTransaction(UUID merchantId,TransactionRequest newTransaction,UserDto userDto);
}
