package com.example.emerchantpay.payment.entities;

public enum UserType {
    ADMIN,
    MERCHANT;

    public static class Values {
        public static final String ADMIN = "ADMIN";
        public static final String MERCHANT = "MERCHANT";
    }
}
