package com.example.emerchantpay.payment.repositories;

import com.example.emerchantpay.payment.entities.AuthorizeTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.UUID;

public interface AuthorizedTransactionRepository extends JpaRepository<AuthorizeTransaction, UUID> {

    AuthorizeTransaction findByIdAndAmount(UUID id, BigDecimal amount);
}
