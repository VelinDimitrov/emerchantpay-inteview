package com.example.emerchantpay.payment.adapters;

import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MerchantAdapter {
    MerchantAdapter INSTANCE = Mappers.getMapper( MerchantAdapter.class );

    MerchantResponse entityToResponse(Merchant merchant);

    @Mapping(target = "password",ignore = true)
    Merchant requestToEntity(MerchantRequest merchantRequest);

    Merchant mergeNewDataWithExistingEntity(MerchantUpdateRequest newMerchantData, @MappingTarget Merchant existingMerchant);
}
