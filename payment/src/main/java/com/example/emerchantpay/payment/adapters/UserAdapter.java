package com.example.emerchantpay.payment.adapters;

import com.example.emerchantpay.payment.entities.User;
import com.example.emerchantpay.payment.pojos.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserAdapter {
    UserAdapter INSTANCE = Mappers.getMapper( UserAdapter.class );

    UserDto entityToDto(User user);
}
