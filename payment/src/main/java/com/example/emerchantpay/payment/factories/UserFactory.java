package com.example.emerchantpay.payment.factories;

import com.example.emerchantpay.payment.entities.AdminUser;
import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.entities.User;
import com.example.emerchantpay.payment.entities.UserType;
import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserFactory {
    public static User getUser(UserCsvCreationDto userCsvCreationDto){
        UserType userType = UserType.valueOf(userCsvCreationDto.getUserType());
        switch (userType){
            case ADMIN:
                return new AdminUser(userCsvCreationDto);
            case MERCHANT:
                return new Merchant(userCsvCreationDto);
            default:
                throw new IllegalArgumentException("Not valid user type");
        }

    }
}
