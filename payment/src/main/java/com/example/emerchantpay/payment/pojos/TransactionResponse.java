package com.example.emerchantpay.payment.pojos;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class TransactionResponse {
    private UUID id;
    private BigDecimal amount;
    private String transactionType;
    private String status;
    private String customerEmail;
    private String customerPhone;
    private LocalDateTime transactionDate;
}
