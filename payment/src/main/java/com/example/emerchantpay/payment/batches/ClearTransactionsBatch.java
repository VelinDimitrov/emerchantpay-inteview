package com.example.emerchantpay.payment.batches;

import com.example.emerchantpay.payment.entities.ChargeTransaction;
import com.example.emerchantpay.payment.entities.RefundTransaction;
import com.example.emerchantpay.payment.entities.TransactionStatus;
import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.repositories.TransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ClearTransactionsBatch {

    private final TransactionRepository transactionRepository;
    private final MerchantService merchantService;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void clearTransactionMoreThanHourOld() {
        LocalDateTime nowTime = LocalDateTime.now();
        log.info("Transaction clean up started! Time : "+nowTime);

        LocalDateTime timeAnHourEarlier = nowTime.minusHours(1);
        var allTransactionsMoreThanAnHour = transactionRepository.findAllByTransactionDateBefore(timeAnHourEarlier);
        log.info("Number of transactions for deletion: "+ allTransactionsMoreThanAnHour.size());

        allTransactionsMoreThanAnHour.forEach(transaction -> {
            transactionRepository.findAllByReferenceId(transaction.getId()).forEach(transactionWithReferenceToCurrentTransaction -> transactionWithReferenceToCurrentTransaction.setReference(null));

            TransactionType transactionType = transaction.getTransactionType();
            boolean isNotError = transaction.getStatus() != TransactionStatus.ERROR;
            if (transactionType == TransactionType.CHARGE && isNotError){
                merchantService.subtractAmountToMerchant(transaction.getMerchant().getId(),((ChargeTransaction)transaction).getAmount());
            }else if (transactionType == TransactionType.REFUND&& isNotError){
                merchantService.addAmountToMerchant(transaction.getMerchant().getId(),((RefundTransaction)transaction).getAmount());
            }
            transactionRepository.delete(transaction);
        });

        log.info("Transaction clean up finished!");
    }
}
