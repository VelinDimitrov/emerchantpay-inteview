package com.example.emerchantpay.payment.entities;

import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
@DiscriminatorValue(UserType.Values.MERCHANT)
public class Merchant extends User{

    @Column
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private MerchantStatus status;

    @Column
    private BigDecimal totalTransactionSum = BigDecimal.ZERO;

    @OneToMany(mappedBy = "merchant",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private List<Transaction> transactions = new ArrayList<>();

    public Merchant(UserCsvCreationDto userCsvCreationDto) {
        super(null,userCsvCreationDto.getEmail(),null, UserType.valueOf(userCsvCreationDto.getUserType()),userCsvCreationDto.getName());
        this.description=userCsvCreationDto.getDescription();
        this.status=MerchantStatus.valueOf(userCsvCreationDto.getStatus());
    }
}
