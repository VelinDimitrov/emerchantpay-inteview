package com.example.emerchantpay.payment.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="transaction_type",
        discriminatorType = DiscriminatorType.STRING)
public class Transaction {

    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue
    private UUID id;

    @Column
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @Column(name="transaction_type",insertable = false,updatable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column
    private String customerEmail;

    @Column
    private String customerPhone;

    @Column
    private LocalDateTime transactionDate;

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Merchant merchant;

    @OneToOne
    private Transaction reference;
}
