package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.adapters.MerchantAdapter;
import com.example.emerchantpay.payment.exceptions.AuthorizationException;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.exceptions.MerchantNotFoundException;
import com.example.emerchantpay.payment.exceptions.TransactionsForMerchantFoundException;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.repositories.MerchantRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.nio.CharBuffer;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class MerchantServiceImpl implements MerchantService {
    private final MerchantRepository merchantRepository;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<MerchantResponse> getMerchants( UserDto userDto) {
        if (!userService.isAdmin(userDto)) {
            throw new AuthorizationException("Only admins can view all merchants");
        }
        return merchantRepository.findAll().stream().map(this::mapEntityToResponse).collect(Collectors.toList());
    }

    @Override
    public MerchantResponse getMerchant(UUID id,UserDto userDto) {
        if (userService.isDifferentMerchant(userDto,id)){
            throw new AuthorizationException("Merchant can only see his details");
        }
        return mapEntityToResponse(findMerchant(id));
    }

    private Merchant findMerchant(UUID id) {
        return merchantRepository.findById(id).orElseThrow(() -> new MerchantNotFoundException(id));
    }

    @Override
    public Merchant createMerchant(MerchantRequest requestObject) {
        Merchant merchantWithSameEmail = merchantRepository.findByEmail(requestObject.getEmail());
        if (merchantWithSameEmail != null) {
            throw new IllegalArgumentException("Merchant with this email exists!  Email: " + requestObject.getEmail());
        }
        Merchant newMerchant = mapRequestToEntity(requestObject);
        newMerchant.setPassword(passwordEncoder.encode(CharBuffer.wrap(requestObject.getPassword())));
       return merchantRepository.save(newMerchant);
    }

    @Override
    public MerchantResponse updateMerchant(UUID id, MerchantUpdateRequest requestObject, UserDto userDto) {
        if (userService.isDifferentMerchant(userDto,id)){
            throw new AuthorizationException("Merchant can only modify their own details");
        }
        Merchant existingMerchant = findMerchant(id);
        Merchant mappedMerchant = MerchantAdapter.INSTANCE.mergeNewDataWithExistingEntity(requestObject, existingMerchant);
        return mapEntityToResponse(mappedMerchant);
    }

    @Override
    public void deleteMerchant(UUID id,UserDto userDto) {
        if (!userService.isAdmin(userDto)) {
            throw new AuthorizationException("Only admins can delete Merchants!");
        }
        Merchant existingMerchant = findMerchant(id);
        if (!existingMerchant.getTransactions().isEmpty()) {
            throw new TransactionsForMerchantFoundException(id);
        }
        merchantRepository.delete(existingMerchant);
    }

    @Override
    public void addAmountToMerchant(UUID merchantId, BigDecimal amount) {
        Merchant merchant = findMerchant(merchantId);
        merchant.setTotalTransactionSum(merchant.getTotalTransactionSum().add(amount));
    }

    @Override
    public void subtractAmountToMerchant(UUID merchantId, BigDecimal amount) {
        Merchant merchant = findMerchant(merchantId);
        merchant.setTotalTransactionSum(merchant.getTotalTransactionSum().subtract(amount));
    }

    private MerchantResponse mapEntityToResponse(Merchant merchant) {
        return MerchantAdapter.INSTANCE.entityToResponse(merchant);
    }

    private Merchant mapRequestToEntity(MerchantRequest merchantRequest) {
        return MerchantAdapter.INSTANCE.requestToEntity(merchantRequest);
    }
}
