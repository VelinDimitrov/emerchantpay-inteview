package com.example.emerchantpay.payment.adapters;

import com.example.emerchantpay.payment.entities.AuthorizeTransaction;
import com.example.emerchantpay.payment.entities.ChargeTransaction;
import com.example.emerchantpay.payment.entities.RefundTransaction;
import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TransactionAdapter {
    TransactionAdapter INSTANCE = Mappers.getMapper( TransactionAdapter.class );

    TransactionResponse entityToResponse(Transaction transaction);

    TransactionResponse entityToResponse(AuthorizeTransaction transaction);

    TransactionResponse entityToResponse(RefundTransaction transaction);

    TransactionResponse entityToResponse(ChargeTransaction transaction);
}
