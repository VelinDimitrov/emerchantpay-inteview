package com.example.emerchantpay.payment.utils;

import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {

    public static List<UserCsvCreationDto> csvToUserCreationRequests(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<UserCsvCreationDto> users = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                UserCsvCreationDto userRequest = UserCsvCreationDto
                        .builder()
                        .name(csvRecord.get(0))
                        .description(csvRecord.get(1))
                        .email(csvRecord.get(2))
                        .password(csvRecord.get(3))
                        .status(csvRecord.get(4))
                        .userType(csvRecord.get(5))
                        .build();

                users.add(userRequest);
            }
            return users;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
