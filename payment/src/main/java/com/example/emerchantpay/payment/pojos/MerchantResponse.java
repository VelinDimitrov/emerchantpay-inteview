package com.example.emerchantpay.payment.pojos;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
public class MerchantResponse {
    private UUID id;
    private String name;
    private String description;
    private String email;
    private String status;
    private BigDecimal totalTransactionSum;
}
