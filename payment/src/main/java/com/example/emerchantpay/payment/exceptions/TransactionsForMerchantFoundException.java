package com.example.emerchantpay.payment.exceptions;

import java.util.UUID;

@java.lang.SuppressWarnings("squid:MaximumInheritanceDepth")
public class TransactionsForMerchantFoundException extends RuntimeException{
    private static final String ERROR_MESSAGE_FORMAT = "Merchant with id: %s has transactions and cannot be deleted!";

    public TransactionsForMerchantFoundException(UUID id){
        super(String.format(ERROR_MESSAGE_FORMAT,id));
    }
}
