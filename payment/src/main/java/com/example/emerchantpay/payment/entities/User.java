package com.example.emerchantpay.payment.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "application_user")
@DiscriminatorColumn(name="user_type",
        discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.JOINED)
public class User {
    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue
    private UUID id;

    @Column(unique = true,nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(name="user_type",insertable = false,updatable = false)
    @Enumerated(EnumType.STRING)
    private UserType userType;

    @Column
    private String name;
}
