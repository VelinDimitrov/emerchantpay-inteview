package com.example.emerchantpay.payment.pojos;

import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.validation.annotation.ValueOfEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {

    @DecimalMin(value = "0.0", inclusive = false,message = "Amount should be bigger than 0")
    private BigDecimal amount;

    @NotNull(message = "Transaction type is mandatory")
    @ValueOfEnum(enumClass = TransactionType.class)
    private String transactionType;

    @NotBlank(message = "Customer email is mandatory")
    @Email(message="Please provide a valid email address")
    private String customerEmail;

    @NotBlank(message = "Customer phone is mandatory")
    private String customerPhone;

    private UUID referenceId;
}
