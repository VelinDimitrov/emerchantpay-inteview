package com.example.emerchantpay.payment.entities;

import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue(UserType.Values.ADMIN)
public class AdminUser extends User{
    public AdminUser(UserCsvCreationDto userCsvCreationDto) {
        super(null,userCsvCreationDto.getEmail(),null, UserType.valueOf(userCsvCreationDto.getUserType()),userCsvCreationDto.getName());
    }
}
