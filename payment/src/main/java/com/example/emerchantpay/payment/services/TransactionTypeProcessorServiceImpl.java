package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.entities.AuthorizeTransaction;
import com.example.emerchantpay.payment.entities.ChargeTransaction;
import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.entities.TransactionStatus;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.repositories.AuthorizedTransactionRepository;
import com.example.emerchantpay.payment.repositories.ChargeTransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import com.example.emerchantpay.payment.services.interfaces.TransactionTypeProcessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TransactionTypeProcessorServiceImpl implements TransactionTypeProcessorService {
    private final AuthorizedTransactionRepository authorizedTransactionRepository;
    private final MerchantService merchantService;
    private final ChargeTransactionRepository chargeTransactionRepository;

    @Override
    public Transaction processTransaction(UUID merchantId, TransactionRequest transactionRequestData, Transaction newTransaction) {
        TransactionType newTransactionType = TransactionType.valueOf(transactionRequestData.getTransactionType());
        switch (newTransactionType) {
            case AUTHORIZE:
                return processAuthorized(transactionRequestData, newTransaction);
            case CHARGE:
                return processCharge(merchantId, transactionRequestData, newTransaction);
            case REFUND:
                return processRefund(merchantId, transactionRequestData, newTransaction);
            case REVERSAL:
                return processReversal(transactionRequestData, newTransaction);
            default:
                throw new IllegalArgumentException("Transaction type is not valid!");
        }
    }


    private Transaction processAuthorized(TransactionRequest transactionRequestData, Transaction newTransaction) {
        TransactionStatus resultStatus = TransactionStatus.ERROR;
        if (transactionRequestData.getReferenceId() == null) {
            resultStatus = TransactionStatus.APPROVED;
        } else {
            log.info("Authorized transaction cannot have reference!");
        }
        newTransaction.setStatus(resultStatus);
        return newTransaction;
    }


    private Transaction processCharge(UUID merchantId, TransactionRequest transactionRequestData, Transaction newTransaction) {
        TransactionStatus resultStatus = TransactionStatus.ERROR;

        UUID referenceId = transactionRequestData.getReferenceId();
        if (referenceId != null) {
            AuthorizeTransaction authorizeTransaction = authorizedTransactionRepository.findByIdAndAmount(referenceId, transactionRequestData.getAmount());

            if (authorizeTransaction != null && authorizeTransaction.getStatus() == TransactionStatus.APPROVED) {
                merchantService.addAmountToMerchant(merchantId, transactionRequestData.getAmount());
                resultStatus = TransactionStatus.APPROVED;
                newTransaction.setReference(authorizeTransaction);
            } else {
                log.info("Charge transaction should reference authorized transaction with same amount and status APPROVED!");
            }
        } else {
            log.info("Charge transaction should have reference!");
        }
        newTransaction.setStatus(resultStatus);
        return newTransaction;
    }

    private Transaction processRefund(UUID merchantId, TransactionRequest transactionRequestData, Transaction newTransaction) {
        TransactionStatus resultStatus = TransactionStatus.ERROR;

        UUID referenceId = transactionRequestData.getReferenceId();
        if (referenceId != null) {
            ChargeTransaction chargeTransaction = chargeTransactionRepository.findById(transactionRequestData.getReferenceId()).orElse(null);
            if (chargeTransaction != null && chargeTransaction.getStatus() == TransactionStatus.APPROVED) {
                chargeTransaction.setStatus(TransactionStatus.REFUNDED);
                newTransaction.setReference(chargeTransaction);
                resultStatus = TransactionStatus.APPROVED;
                merchantService.subtractAmountToMerchant(merchantId, transactionRequestData.getAmount());
            } else {
                log.info("Refund transaction should reference charged transaction in status approved!");
            }
        } else {
            log.info("Refund transaction should have reference!");
        }

        newTransaction.setStatus(resultStatus);
        return newTransaction;
    }

    private Transaction processReversal(TransactionRequest transactionRequestData, Transaction newTransaction) {
        TransactionStatus resultStatus = TransactionStatus.ERROR;

        UUID referenceId = transactionRequestData.getReferenceId();
        if (referenceId != null) {
            AuthorizeTransaction authorizeTransaction = authorizedTransactionRepository.findById(transactionRequestData.getReferenceId()).orElse(null);
            if (authorizeTransaction != null && authorizeTransaction.getStatus() == TransactionStatus.APPROVED) {
                authorizeTransaction.setStatus(TransactionStatus.REVERSED);
                newTransaction.setReference(authorizeTransaction);
                resultStatus = TransactionStatus.APPROVED;
            } else {
                log.info("Reversal transaction does not have reference!");
            }
        } else {
            log.info("Reversal transaction should have reference!");
        }

        newTransaction.setStatus(resultStatus);
        return newTransaction;
    }
}
