package com.example.emerchantpay.payment.entities;

import com.example.emerchantpay.payment.pojos.TransactionRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DiscriminatorValue(TransactionType.Values.CHARGE)
public class ChargeTransaction extends  Transaction{
    @Column
    private BigDecimal amount;

    public ChargeTransaction(TransactionRequest request){
        super(null,null,TransactionType.CHARGE,request.getCustomerEmail(),request.getCustomerPhone(),null,null,null);
        this.amount = request.getAmount();
    }
}
