package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/csv/upload")
public class CSVUploadEndpoint {
    private final UserService userService;

    @ApiOperation(value = "Import users from CSV", notes = "Imports users in the following format : Name;Description;Email;Password;Status;Type")
    @PostMapping("/users")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @AuthenticationPrincipal UserDto userDto) throws IOException {
        userService.uploadUserFromCsv(file,userDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
