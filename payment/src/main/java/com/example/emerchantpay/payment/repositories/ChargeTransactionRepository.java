package com.example.emerchantpay.payment.repositories;

import com.example.emerchantpay.payment.entities.ChargeTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ChargeTransactionRepository extends JpaRepository<ChargeTransaction, UUID> {
}
