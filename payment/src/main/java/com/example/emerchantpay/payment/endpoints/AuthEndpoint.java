package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.configurations.UserAuthenticationProvider;
import com.example.emerchantpay.payment.pojos.CredentialsDto;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class AuthEndpoint {
    private final UserService userService;
    private final UserAuthenticationProvider authenticationProvider;

    @ApiOperation(value = "Authenticate user")
    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody CredentialsDto credentialsDto) {
        UserDto userDto = userService.login(credentialsDto);
        userDto.setToken(authenticationProvider.createToken(userDto.getEmail()));
        return ResponseEntity.ok(userDto);
    }
}
