package com.example.emerchantpay.payment.repositories;

import com.example.emerchantpay.payment.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
    List<Transaction> findAllByTransactionDateBefore(LocalDateTime dateBefore);

    List<Transaction> findAllByMerchantIdOrderByTransactionDate(UUID merchantId);

    List<Transaction> findAllByReferenceId(UUID referenceId);
}
