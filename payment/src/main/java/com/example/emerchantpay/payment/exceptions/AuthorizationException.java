package com.example.emerchantpay.payment.exceptions;

@java.lang.SuppressWarnings("squid:MaximumInheritanceDepth")
public class AuthorizationException extends RuntimeException{
    public AuthorizationException(String errorMessage){
        super(errorMessage);
    }
}
