package com.example.emerchantpay.payment.services.interfaces;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import com.example.emerchantpay.payment.pojos.UserDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface MerchantService {
    List<MerchantResponse> getMerchants(UserDto userDto);

    MerchantResponse getMerchant(UUID id,UserDto userDto);

    Merchant createMerchant(MerchantRequest requestObject);

    MerchantResponse updateMerchant(UUID id, MerchantUpdateRequest requestObject, UserDto userDto);

    void deleteMerchant(UUID id,UserDto userDto);

    /**
     * Add amount to merchant total transaction sum.
     *
     * @param merchantId the merchant id
     * @param amount     the amount
     */
    void addAmountToMerchant(UUID merchantId, BigDecimal amount);

    /**
     * Subtract amount from merchant total transaction sum.
     *
     * @param merchantId the merchant id
     * @param amount     the amount
     */
    void subtractAmountToMerchant(UUID merchantId, BigDecimal amount);
}
