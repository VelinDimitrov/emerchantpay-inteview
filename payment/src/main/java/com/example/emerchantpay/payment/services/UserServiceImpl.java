package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.adapters.UserAdapter;
import com.example.emerchantpay.payment.entities.User;
import com.example.emerchantpay.payment.entities.UserType;
import com.example.emerchantpay.payment.exceptions.AuthorizationException;
import com.example.emerchantpay.payment.factories.UserFactory;
import com.example.emerchantpay.payment.pojos.CredentialsDto;
import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.repositories.UserRepository;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import com.example.emerchantpay.payment.utils.CSVHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDto findByEmail(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new IllegalArgumentException("User not found by email!"));
        return UserAdapter.INSTANCE.entityToDto(user);
    }

    @Override
    public UserDto login(CredentialsDto credentialsDto) {
        User user = userRepository.findByEmail(credentialsDto.getEmail())
                .orElseThrow(() -> new AuthorizationException("Unknown user with email: " + credentialsDto.getEmail()));

        if (passwordEncoder.matches(CharBuffer.wrap(credentialsDto.getPassword()), user.getPassword())) {
            return UserAdapter.INSTANCE.entityToDto(user);
        }
        throw new AuthorizationException("Invalid password");
    }

    @Override
    public void uploadUserFromCsv(MultipartFile file, UserDto userDto) throws IOException {
        if (!isAdmin(userDto)) {
            throw new AuthorizationException("Only admins can upload CSV");
        }
        List<UserCsvCreationDto> usersForCreation = CSVHelper.csvToUserCreationRequests(file.getInputStream());
        for (UserCsvCreationDto newUser : usersForCreation) {
            try {
                Set<ConstraintViolation<UserCsvCreationDto>> violations = VALIDATOR.validate(newUser);
                if (violations.isEmpty()){
                    User user = UserFactory.getUser(newUser);
                    user.setPassword(passwordEncoder.encode(CharBuffer.wrap(newUser.getPassword())));
                    userRepository.save(user);
                    continue;
                }

                throw new IllegalArgumentException("Bad formatting of data : "+violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining()));
            } catch (IllegalArgumentException e) {
                log.warn(e.getMessage());
            }
        }
    }

    @Override
    public boolean isAdmin(UserDto userDto) {
        return UserType.ADMIN.name().equals(userDto.getUserType()) && userDto.getId() != null;
    }

    @Override
    public boolean isDifferentMerchant(UserDto userDto, UUID merchantId) {
        return UserType.MERCHANT.name().equals(userDto.getUserType()) && !merchantId.equals(userDto.getId());
    }
}
