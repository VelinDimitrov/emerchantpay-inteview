package com.example.emerchantpay.payment.services.interfaces;

import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.pojos.TransactionRequest;

import java.util.UUID;

public interface TransactionTypeProcessorService {
    Transaction processTransaction(UUID merchantId,TransactionRequest transactionRequestData, Transaction newTransaction);
}
