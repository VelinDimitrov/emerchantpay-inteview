package com.example.emerchantpay.payment.entities;

import com.example.emerchantpay.payment.pojos.TransactionRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@Entity
@DiscriminatorValue(TransactionType.Values.REVERSAL)
public class ReversalTransaction extends  Transaction{

    public ReversalTransaction(TransactionRequest request){
        super(null,null,TransactionType.REVERSAL,request.getCustomerEmail(),request.getCustomerPhone(),null,null,null);
    }
}
