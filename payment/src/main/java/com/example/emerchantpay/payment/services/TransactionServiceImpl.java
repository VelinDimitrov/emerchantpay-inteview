package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.adapters.TransactionAdapter;
import com.example.emerchantpay.payment.entities.*;
import com.example.emerchantpay.payment.exceptions.AuthorizationException;
import com.example.emerchantpay.payment.exceptions.MerchantNotFoundException;
import com.example.emerchantpay.payment.exceptions.NoActiveMerchantFoundException;
import com.example.emerchantpay.payment.factories.TransactionFactory;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.repositories.MerchantRepository;
import com.example.emerchantpay.payment.repositories.TransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.TransactionService;
import com.example.emerchantpay.payment.services.interfaces.TransactionTypeProcessorService;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final MerchantRepository merchantRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionTypeProcessorService transactionTypeProcessorService;
    private final UserService userService;

    @Override
    public List<TransactionResponse> getTransactionsForMerchant(UUID merchantId, UserDto userDto) {
        if (userService.isDifferentMerchant(userDto,merchantId)){
            throw new AuthorizationException("Merchant can only see his transactions");
        }
        Optional<Merchant> optionalMerchant = merchantRepository.findById(merchantId);
        if (optionalMerchant.isEmpty()){
            throw new MerchantNotFoundException(merchantId);
        }
        return transactionRepository.findAllByMerchantIdOrderByTransactionDate(merchantId).stream().map(this::mapEntityToResponse).collect(Collectors.toList());
    }

    @Override
    public TransactionResponse createTransaction(UUID merchantId, TransactionRequest transactionRequestData,UserDto userDto) {
        if (userService.isDifferentMerchant(userDto,merchantId)){
            throw new AuthorizationException("Merchant can only create transactions to himself");
        }
        var merchant = merchantRepository.findByIdAndStatus(merchantId, MerchantStatus.ACTIVE);
        if (merchant == null) {
            throw new NoActiveMerchantFoundException(merchantId);
        }
        Transaction newTransaction = TransactionFactory.getTransaction(transactionRequestData);
        newTransaction.setTransactionDate(LocalDateTime.now());
        newTransaction.setMerchant(merchant);
        transactionTypeProcessorService.processTransaction(merchantId, transactionRequestData, newTransaction);

       return mapEntityToResponse(transactionRepository.save(newTransaction));
    }

    private TransactionResponse mapEntityToResponse(Transaction transaction) {
        if (transaction instanceof AuthorizeTransaction) {
            return TransactionAdapter.INSTANCE.entityToResponse((AuthorizeTransaction) transaction);
        } else if (transaction instanceof RefundTransaction) {
            return TransactionAdapter.INSTANCE.entityToResponse((RefundTransaction) transaction);
        } else if (transaction instanceof ChargeTransaction) {
            return TransactionAdapter.INSTANCE.entityToResponse((ChargeTransaction) transaction);
        }
        return TransactionAdapter.INSTANCE.entityToResponse(transaction);
    }

}
