package com.example.emerchantpay.payment.services.interfaces;

import com.example.emerchantpay.payment.pojos.CredentialsDto;
import com.example.emerchantpay.payment.pojos.UserDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

public interface UserService {

    UserDto findByEmail(String email);

    UserDto login(CredentialsDto credentialsDto);

    void uploadUserFromCsv(MultipartFile file, UserDto userDto) throws IOException;

    boolean isAdmin(UserDto userDto);

    boolean isDifferentMerchant(UserDto userDto, UUID merchantId);
}
