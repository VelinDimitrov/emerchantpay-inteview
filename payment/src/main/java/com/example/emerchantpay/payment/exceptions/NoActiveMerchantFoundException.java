package com.example.emerchantpay.payment.exceptions;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@java.lang.SuppressWarnings("squid:MaximumInheritanceDepth")
public class NoActiveMerchantFoundException extends EntityNotFoundException {
    private static final String ERROR_MESSAGE_FORMAT = "No active Merchant found for id: %s";

    public NoActiveMerchantFoundException(UUID id){
        super(String.format(ERROR_MESSAGE_FORMAT,id));
    }
}
