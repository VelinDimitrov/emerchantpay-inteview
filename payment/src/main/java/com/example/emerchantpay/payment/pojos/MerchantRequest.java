package com.example.emerchantpay.payment.pojos;

import com.example.emerchantpay.payment.entities.MerchantStatus;
import com.example.emerchantpay.payment.validation.annotation.ValueOfEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MerchantRequest {
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Description is mandatory")
    private String description;

    @NotBlank(message = "Email is mandatory")
    @Email(message="Please provide a valid email address")
    private String email;

    @NotBlank(message = "Status is mandatory")
    @ValueOfEnum(enumClass = MerchantStatus.class)
    private String status;

    @NotBlank(message = "Password is mandatory")
    @Size(min = 6,message = "Password should be bigger than 6 chars")
    private String password;
}
