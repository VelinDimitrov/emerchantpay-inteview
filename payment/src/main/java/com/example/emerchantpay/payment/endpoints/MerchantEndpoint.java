package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/merchants")
public class MerchantEndpoint {
    private final MerchantService merchantService;

    @ApiOperation(value = "Get all merchants")
    @GetMapping
    public ResponseEntity<List<MerchantResponse>> getAll(@AuthenticationPrincipal UserDto userDto){
        return ResponseEntity.ok(merchantService.getMerchants(userDto));
    }

    @ApiOperation(value = "Get merchant by id")
    @GetMapping("/{id}")
    public ResponseEntity<MerchantResponse> getMerchant(@PathVariable UUID id,@AuthenticationPrincipal UserDto userDto){
        return ResponseEntity.ok(merchantService.getMerchant(id,userDto));
    }

    @ApiOperation(value = "Create merchant")
    @PostMapping
    public ResponseEntity<MerchantResponse> createMerchant(@Valid @RequestBody MerchantRequest newMerchant) {
        Merchant merchant = merchantService.createMerchant(newMerchant);
        return ResponseEntity.created(URI.create("/merchants/"+ merchant.getId())).build();
    }

    @ApiOperation(value = "Update merchant")
    @PutMapping("/{id}")
    public ResponseEntity<MerchantResponse> updateMerchant(@PathVariable UUID id, @Valid @RequestBody MerchantUpdateRequest newMerchantData, @AuthenticationPrincipal UserDto userDto) {
            return ResponseEntity.ok(merchantService.updateMerchant(id,newMerchantData,userDto));
    }

    @ApiOperation(value = "Delete merchant")
    @DeleteMapping("/{id}")
    public ResponseEntity<MerchantResponse> deleteMerchant(@PathVariable UUID id, @AuthenticationPrincipal UserDto userDto){
        merchantService.deleteMerchant(id,userDto);
        return ResponseEntity.noContent().build();
    }
}
