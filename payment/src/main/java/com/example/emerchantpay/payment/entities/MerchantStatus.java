package com.example.emerchantpay.payment.entities;

public enum MerchantStatus {
    ACTIVE,
    INACTIVE
}
