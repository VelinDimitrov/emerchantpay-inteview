package com.example.emerchantpay.payment.entities;

public enum TransactionStatus {
    APPROVED,
    REVERSED,
    REFUNDED,
    ERROR
}
