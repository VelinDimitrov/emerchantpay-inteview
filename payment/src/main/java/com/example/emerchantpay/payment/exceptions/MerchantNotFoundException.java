package com.example.emerchantpay.payment.exceptions;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@java.lang.SuppressWarnings("squid:MaximumInheritanceDepth")
public class MerchantNotFoundException extends EntityNotFoundException {
    private static final String ERROR_MESSAGE_FORMAT = "Merchant not found for id: %s";

   public MerchantNotFoundException(UUID id){
       super(String.format(ERROR_MESSAGE_FORMAT,id));
   }
}
