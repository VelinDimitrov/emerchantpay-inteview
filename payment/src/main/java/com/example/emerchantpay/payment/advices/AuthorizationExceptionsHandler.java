package com.example.emerchantpay.payment.advices;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.example.emerchantpay.payment.exceptions.AuthorizationException;
import com.example.emerchantpay.payment.pojos.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthorizationExceptionsHandler {
    @ExceptionHandler(value = JWTVerificationException.class)
    public ResponseEntity<ErrorResponse> exception(JWTVerificationException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = AuthorizationException.class)
    public ResponseEntity<ErrorResponse> exception(AuthorizationException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.UNAUTHORIZED);
    }
}
