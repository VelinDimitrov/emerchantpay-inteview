package com.example.emerchantpay.payment.entities;

public enum TransactionType {
    AUTHORIZE,
    CHARGE,
    REFUND,
    REVERSAL;

    public static class Values {
        public static final String AUTHORIZE = "AUTHORIZE";
        public static final String CHARGE = "CHARGE";
        public static final String REFUND = "REFUND";
        public static final String REVERSAL = "REVERSAL";
    }
}
