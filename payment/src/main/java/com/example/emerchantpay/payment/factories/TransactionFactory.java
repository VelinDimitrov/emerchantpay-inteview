package com.example.emerchantpay.payment.factories;

import com.example.emerchantpay.payment.entities.*;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.entities.TransactionType;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionFactory {

    public static Transaction getTransaction(TransactionRequest request){
        TransactionType transactionType = TransactionType.valueOf(request.getTransactionType());
        switch (transactionType){
            case CHARGE:
                return new ChargeTransaction(request);
            case REFUND:
                return new RefundTransaction(request);
            case REVERSAL:
                return new ReversalTransaction(request);
            case AUTHORIZE:
                return new AuthorizeTransaction(request);
            default:
                throw new IllegalArgumentException("Not valid transaction");
        }

    }

}
