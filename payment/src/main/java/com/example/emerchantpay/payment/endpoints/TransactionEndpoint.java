package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.services.interfaces.TransactionService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/merchants/{merchantId}/transactions")
public class TransactionEndpoint {
    private final TransactionService transactionService;

    @ApiOperation(value = "Get all transactions for merchant")
    @GetMapping
    public ResponseEntity<List<TransactionResponse>> getTransactionsForMerchant(@PathVariable UUID merchantId,@AuthenticationPrincipal UserDto userDto){
        return ResponseEntity.ok(transactionService.getTransactionsForMerchant(merchantId,userDto));
    }

    @ApiOperation(value = "Create transaction")
    @PostMapping
    public ResponseEntity<TransactionResponse> createTransaction(@PathVariable UUID merchantId,@Valid @RequestBody TransactionRequest newTransaction,@AuthenticationPrincipal UserDto userDto) {
        TransactionResponse transaction = transactionService.createTransaction(merchantId, newTransaction, userDto);
        return ResponseEntity.created(URI.create("/merchants/"+transaction.getId()+"/transactions")).build();
    }
}
