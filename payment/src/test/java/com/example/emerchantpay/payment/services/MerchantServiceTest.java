package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.entities.MerchantStatus;
import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.exceptions.AuthorizationException;
import com.example.emerchantpay.payment.exceptions.MerchantNotFoundException;
import com.example.emerchantpay.payment.exceptions.TransactionsForMerchantFoundException;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import com.example.emerchantpay.payment.pojos.UserDto;
import com.example.emerchantpay.payment.repositories.MerchantRepository;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MerchantServiceTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    private static final String MERCHANT_EMAIL = "email@gmail.com";
    private static final String MERCHANT_NAME = "Pesho";
    private static final String MERCHANT_DESCRIPTION = "test description";

    @Mock
    private MerchantRepository merchantRepository;
    @Mock
    private UserService userService;
    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private MerchantServiceImpl merchantService;

    private Merchant merchant;
    private MerchantRequest merchantRequest;
    private UserDto userDto;

    @BeforeEach
    public void setUp() {
        merchant = new Merchant();
        merchantRequest = new MerchantRequest();
        userDto = new UserDto();
    }

    @Test
    void getMerchantTest() {
        merchant.setEmail(MERCHANT_EMAIL);
        merchant.setName(MERCHANT_NAME);
        merchant.setDescription(MERCHANT_DESCRIPTION);
        merchant.setStatus(MerchantStatus.INACTIVE);
        merchant.setTotalTransactionSum(BigDecimal.TEN);
        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));

        MerchantResponse result = merchantService.getMerchant(MERCHANT_ID,null);
        Mockito.verify(merchantRepository).findById(MERCHANT_ID);

        MerchantResponse expectedResponse = MerchantResponse.builder().email(MERCHANT_EMAIL).name(MERCHANT_NAME).description(MERCHANT_DESCRIPTION).status(MerchantStatus.INACTIVE.name()).totalTransactionSum(BigDecimal.TEN).build();
        Assertions.assertThat(result).isNotNull().isEqualTo(expectedResponse);
    }

    @Test
    void getMerchantTest_throwExceptionOnMissingMerchant() {
        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.empty());
        assertThrows(MerchantNotFoundException.class, () -> merchantService.getMerchant(MERCHANT_ID,null));
    }

    @Test
    void getAllMerchantsTest() {
        merchant.setEmail(MERCHANT_EMAIL);
        merchant.setName(MERCHANT_NAME);
        merchant.setDescription(MERCHANT_DESCRIPTION);
        merchant.setStatus(MerchantStatus.INACTIVE);
        merchant.setTotalTransactionSum(BigDecimal.ONE);

        UUID secondMerchantId =UUID.randomUUID();
        Merchant secondMerchant = new Merchant();
        secondMerchant.setId(secondMerchantId);
        secondMerchant.setEmail("test@mail.com");
        secondMerchant.setStatus(MerchantStatus.ACTIVE);
        secondMerchant.setName("secondMerchant");
        secondMerchant.setDescription("description of second merchant");
        secondMerchant.setTotalTransactionSum(BigDecimal.TEN);
        secondMerchant.setTransactions(new ArrayList<>());
        Mockito.when(merchantRepository.findAll()).thenReturn(Arrays.asList(merchant, secondMerchant));

        userDto = new UserDto();
        userDto.setUserType("Admin");
        Mockito.when(userService.isAdmin(userDto)).thenReturn(true);

        List<MerchantResponse> result = merchantService.getMerchants(userDto);
        Mockito.verify(merchantRepository).findAll();

        MerchantResponse firstExpectedMerchant = MerchantResponse.builder()
                .email(MERCHANT_EMAIL)
                .name(MERCHANT_NAME)
                .description(MERCHANT_DESCRIPTION)
                .status(MerchantStatus.INACTIVE.name())
                .totalTransactionSum(BigDecimal.ONE)
                .build();
        MerchantResponse secondExpectedMerchant = MerchantResponse.builder()
                .id(secondMerchantId)
                .email("test@mail.com")
                .name("secondMerchant")
                .description("description of second merchant")
                .status(MerchantStatus.ACTIVE.name())
                .totalTransactionSum(BigDecimal.TEN)
                .build();
        Assertions.assertThat(result).isEqualTo(Arrays.asList(firstExpectedMerchant, secondExpectedMerchant));
    }

    @Test
    void createMerchantTest() {
        Random random = new Random();
        String password = String.valueOf(random.nextInt(8));
        merchantRequest.setEmail(MERCHANT_EMAIL);
        merchantRequest.setName(MERCHANT_NAME);
        merchantRequest.setDescription(MERCHANT_DESCRIPTION);
        merchantRequest.setStatus(MerchantStatus.ACTIVE.name());
        merchantRequest.setPassword(password);

        merchant.setEmail(MERCHANT_EMAIL);
        merchant.setName(MERCHANT_NAME);
        merchant.setDescription(MERCHANT_DESCRIPTION);
        merchant.setStatus(MerchantStatus.INACTIVE);

        merchantService.createMerchant(merchantRequest);
        Mockito.verify(merchantRepository).save(Mockito.any(Merchant.class));
    }

    @Test
    void updateMerchantTest() {
        MerchantUpdateRequest merchantRequest = MerchantUpdateRequest.builder()
                .email(MERCHANT_EMAIL)
                .name(MERCHANT_NAME)
                .description(MERCHANT_DESCRIPTION)
                .status(MerchantStatus.ACTIVE.name())
                .build();

        merchant.setStatus(MerchantStatus.INACTIVE);
        merchant.setTotalTransactionSum(BigDecimal.ONE);
        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));

        MerchantResponse result = merchantService.updateMerchant(MERCHANT_ID, merchantRequest,null);
        MerchantResponse expectedResponse = MerchantResponse.builder()
                .email(MERCHANT_EMAIL)
                .name(MERCHANT_NAME)
                .description(MERCHANT_DESCRIPTION)
                .status(MerchantStatus.ACTIVE.name())
                .totalTransactionSum(BigDecimal.ONE)
                .build();
        Assertions.assertThat(result).isNotNull().isEqualTo(expectedResponse);
    }

    @Test
    void deleteMerchantTest() {
        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));
        Mockito.doNothing().when(merchantRepository).delete(merchant);

        userDto = new UserDto();
        userDto.setUserType("Admin");
        Mockito.when(userService.isAdmin(userDto)).thenReturn(true);

        merchantService.deleteMerchant(MERCHANT_ID,userDto);
        Mockito.verify(merchantRepository).findById(MERCHANT_ID);
        Mockito.verify(merchantRepository).delete(merchant);
    }

    @Test
    void deleteMerchantTest_throwExceptionOnTransactionsForMerchant() {
        merchant.setTransactions(List.of(new Transaction()));
        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));
        userDto = new UserDto();
        userDto.setUserType("Admin");
        Mockito.when(userService.isAdmin(userDto)).thenReturn(true);

        Assertions.assertThatThrownBy(()-> merchantService.deleteMerchant(MERCHANT_ID,userDto))
                .isInstanceOf(TransactionsForMerchantFoundException.class)
                .hasMessageContaining("Merchant with id: 3fa85f64-5717-4562-b3fc-2c963f66afa6 has transactions and cannot be deleted!");
        Mockito.verify(merchantRepository).findById(MERCHANT_ID);
        Mockito.verify(merchantRepository,Mockito.never()).delete(merchant);
    }
    @Test
    void deleteMerchantTest_throwAuthorizationException() {
        userDto = new UserDto();
        userDto.setUserType("Merchant");
        Mockito.when(userService.isAdmin(userDto)).thenReturn(false);

        Assertions.assertThatThrownBy(()-> merchantService.deleteMerchant(MERCHANT_ID,userDto))
                .isInstanceOf(AuthorizationException.class)
                .hasMessageContaining("Only admins can delete Merchants!");
        Mockito.verify(merchantRepository,Mockito.never()).delete(merchant);
    }
}
