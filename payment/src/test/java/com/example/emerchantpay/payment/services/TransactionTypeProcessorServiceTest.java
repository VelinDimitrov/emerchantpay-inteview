package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.entities.*;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.repositories.AuthorizedTransactionRepository;
import com.example.emerchantpay.payment.repositories.ChargeTransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class TransactionTypeProcessorServiceTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    private static final UUID TRANSACTION_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa5");
    private static final UUID REFERENCE_TRANSACTION_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa4");

    @Mock
    private  AuthorizedTransactionRepository authorizedTransactionRepository;
    @Mock
    private  ChargeTransactionRepository chargeTransactionRepository;
    @Mock
    private MerchantService merchantService;
    @InjectMocks
    private TransactionTypeProcessorServiceImpl transactionTypeProcessorService;

    private Transaction transaction;

    @BeforeEach
    public void setUp() {
        transaction = new Transaction();
        transaction.setId(TRANSACTION_ID);
    }

    @Test
    void processAuthorizeTest_referenceTransactionPresent() {
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType( "AUTHORIZE").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
    }

    @Test
    void processAuthorizeTest() {
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType("AUTHORIZE").build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.APPROVED);
    }

    @Test
    void processChargeTest() {
        AuthorizeTransaction authorizeTransaction = new AuthorizeTransaction();
        authorizeTransaction.setStatus(TransactionStatus.APPROVED);
        Mockito.when(authorizedTransactionRepository.findByIdAndAmount(REFERENCE_TRANSACTION_ID,BigDecimal.TEN)).thenReturn(authorizeTransaction);

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("CHARGE").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.APPROVED);
        Assertions.assertThat(transaction.getReference()).isEqualTo(authorizeTransaction);
        Mockito.verify(merchantService).addAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processChargeTest_referenceTransactionNotApprovedStatus() {
        AuthorizeTransaction authorizeTransaction = new AuthorizeTransaction();
        authorizeTransaction.setStatus(TransactionStatus.ERROR);
        Mockito.when(authorizedTransactionRepository.findByIdAndAmount(REFERENCE_TRANSACTION_ID,BigDecimal.TEN)).thenReturn(authorizeTransaction);

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("CHARGE").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Assertions.assertThat(transaction.getReference()).isNull();
        Mockito.verify(merchantService,Mockito.never()).addAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processChargeTest_referenceTransactionNotPresentInDb() {
        Mockito.when(authorizedTransactionRepository.findByIdAndAmount(REFERENCE_TRANSACTION_ID,BigDecimal.TEN)).thenReturn(null);

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("CHARGE").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Assertions.assertThat(transaction.getReference()).isNull();
        Mockito.verify(merchantService,Mockito.never()).addAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processChargeTest_referenceTransactionNotPresentInRequest() {
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("CHARGE").build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Assertions.assertThat(transaction.getReference()).isNull();
        Mockito.verify(merchantService,Mockito.never()).addAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processReversalTest() {
        AuthorizeTransaction authorizeTransaction = new AuthorizeTransaction();
        authorizeTransaction.setStatus(TransactionStatus.APPROVED);
        Mockito.when(authorizedTransactionRepository.findById(REFERENCE_TRANSACTION_ID)).thenReturn(Optional.of(authorizeTransaction));

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("REVERSAL").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(authorizeTransaction.getStatus()).isEqualTo(TransactionStatus.REVERSED);
        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.APPROVED);
        Assertions.assertThat(transaction.getReference()).isEqualTo(authorizeTransaction);
    }

    @Test
    void processReversalTest_referenceTransactionNotPresentInRequest() {
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("REVERSAL").build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Assertions.assertThat(transaction.getReference()).isNull();
    }

    @Test
    void processReversalTest_referenceTransactionNotPresentInDb() {
        Mockito.when(authorizedTransactionRepository.findById(REFERENCE_TRANSACTION_ID)).thenReturn(Optional.empty());
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType( "REVERSAL").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Assertions.assertThat(transaction.getReference()).isNull();
    }

    @Test
    void processRefundedTest() {
        ChargeTransaction chargeTransaction = new ChargeTransaction();
        chargeTransaction.setStatus(TransactionStatus.APPROVED);
        Mockito.when(chargeTransactionRepository.findById(REFERENCE_TRANSACTION_ID)).thenReturn(Optional.of(chargeTransaction));

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("REFUND").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(chargeTransaction.getStatus()).isEqualTo(TransactionStatus.REFUNDED);
        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.APPROVED);
        Assertions.assertThat(transaction.getReference()).isEqualTo(chargeTransaction);
        Mockito.verify(merchantService).subtractAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processRefundedTest_referenceTransactionNotPresentInDb() {
        Mockito.when(chargeTransactionRepository.findById(REFERENCE_TRANSACTION_ID)).thenReturn(Optional.empty());

        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType("REFUND").referenceId(REFERENCE_TRANSACTION_ID).build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Mockito.verify(merchantService,Mockito.never()).subtractAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }

    @Test
    void processRefundedTest_referenceTransactionNotPresentInRequest() {
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.TEN).transactionType( "REFUND").build();
        transactionTypeProcessorService.processTransaction(MERCHANT_ID,transactionRequest,transaction);

        Assertions.assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.ERROR);
        Mockito.verify(merchantService,Mockito.never()).subtractAmountToMerchant(MERCHANT_ID,BigDecimal.TEN);
    }
}
