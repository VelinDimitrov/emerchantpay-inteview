package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.MerchantUpdateRequest;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class MerchantEndpointTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");

    @Mock
    private MerchantService merchantService;

    @InjectMocks
    private MerchantEndpoint merchantEndpoint;

    @Test
    void getMerchantTest() {
        ResponseEntity<MerchantResponse> response = merchantEndpoint.getMerchant(MERCHANT_ID,null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getAllMerchantsTest() {
        ResponseEntity<List<MerchantResponse>> response = merchantEndpoint.getAll(null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void createMerchantTest() {
        Mockito.when(merchantService.createMerchant(MerchantRequest.builder().name("test merchant").build())).thenReturn(new Merchant());
        ResponseEntity<MerchantResponse> response = merchantEndpoint.createMerchant(MerchantRequest.builder().name("test merchant").build());
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void updateMerchantTest() {
        ResponseEntity<MerchantResponse> response = merchantEndpoint.updateMerchant(MERCHANT_ID, MerchantUpdateRequest.builder().build(),null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteMerchantTest() {
        ResponseEntity<MerchantResponse> response = merchantEndpoint.deleteMerchant(MERCHANT_ID,null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}
