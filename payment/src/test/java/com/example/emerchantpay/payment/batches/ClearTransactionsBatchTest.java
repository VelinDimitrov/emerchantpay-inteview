package com.example.emerchantpay.payment.batches;

import com.example.emerchantpay.payment.entities.ChargeTransaction;
import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.repositories.TransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class ClearTransactionsBatchTest {
    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private MerchantService merchantService;

    @InjectMocks
    private ClearTransactionsBatch clearTransactionsBatch;

    private static final UUID TRANSACTION_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa5");
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");

    @Test
    void clearTransactionMoreThanHourOld() {
        ChargeTransaction transaction = new ChargeTransaction();
        transaction.setTransactionDate(LocalDateTime.now().minusHours(2));
        transaction.setId(TRANSACTION_ID);
        transaction.setTransactionType(TransactionType.CHARGE);
        transaction.setMerchant(new Merchant());
        transaction.getMerchant().setId(MERCHANT_ID);
        transaction.setAmount(BigDecimal.valueOf(1000));

        Mockito.when(transactionRepository.findAllByTransactionDateBefore(Mockito.any(LocalDateTime.class))).thenReturn(List.of(transaction));
        Mockito.when(transactionRepository.findAllByReferenceId(transaction.getId())).thenReturn(List.of(transaction));
        Mockito.doNothing().when(merchantService).subtractAmountToMerchant(MERCHANT_ID, transaction.getAmount());
        clearTransactionsBatch.clearTransactionMoreThanHourOld();
        Mockito.verify(transactionRepository).findAllByTransactionDateBefore(Mockito.any(LocalDateTime.class));
        Mockito.verify(transactionRepository).delete(transaction);
    }
}
