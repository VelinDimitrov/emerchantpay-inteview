package com.example.emerchantpay.payment.pojos;

import com.example.emerchantpay.payment.entities.MerchantStatus;
import com.example.emerchantpay.payment.utils.ValidationUtils;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;

import static org.assertj.core.api.Assertions.assertThat;

public class MerchantRequestValidationTest {

    @Test
    public void whenAllInvalid_thenViolationsShouldBeReported() {
        MerchantRequest merchantRequest = MerchantRequest.builder().build();
        var violations = ValidationUtils.getValidator().validate(merchantRequest);

        assertThat(violations.size()).isEqualTo(5);
        assertThat(violations)
                .extracting(ConstraintViolation::getMessage)
                .contains("Password is mandatory","Email is mandatory", "Status is mandatory", "Description is mandatory", "Name is mandatory");

    }

    @Test
    public void whenInvalidStatus_thenViolationsShouldBeReported() {
        MerchantRequest merchantRequest = MerchantRequest.builder().name("test").description("testes").email("test@mail.com").status("different status").password("test123").build();
        var violations = ValidationUtils.getValidator().validate(merchantRequest);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations)
                .extracting(ConstraintViolation::getMessage)
                .contains("Must be any of enum class com.example.emerchantpay.payment.entities.MerchantStatus");
    }

    @Test
    public void whenInvalidPassword_thenViolationsShouldBeReported() {
        MerchantRequest merchantRequest = MerchantRequest.builder().name("test").description("testes").email("test@mail.com").status(MerchantStatus.ACTIVE.name()).password("123").build();
        var violations = ValidationUtils.getValidator().validate(merchantRequest);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations)
                .extracting(ConstraintViolation::getMessage)
                .contains("Password should be bigger than 6 chars");
    }
}
