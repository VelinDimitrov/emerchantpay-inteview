package com.example.emerchantpay.payment.factories;

import com.example.emerchantpay.payment.entities.*;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class TransactionFactoryTest {

    @Test
    public void getTransaction_getRefundTransaction(){
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType(TransactionType.REFUND.name()).build();
        Transaction transaction = TransactionFactory.getTransaction(transactionRequest);

        Assertions.assertThat(transaction).isNotNull().isInstanceOf(RefundTransaction.class);
    }

    @Test
    public void getTransaction_getReversalTransaction(){
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType(TransactionType.REVERSAL.name()).build();
        Transaction transaction = TransactionFactory.getTransaction(transactionRequest);

        Assertions.assertThat(transaction).isNotNull().isInstanceOf(ReversalTransaction.class);
    }

    @Test
    public void getTransaction_getChargeTransaction(){
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType(TransactionType.CHARGE.name()).build();
        Transaction transaction = TransactionFactory.getTransaction(transactionRequest);

        Assertions.assertThat(transaction).isNotNull().isInstanceOf(ChargeTransaction.class);
    }

    @Test
    public void getTransaction_getAuthorizeTransaction(){
        TransactionRequest transactionRequest = TransactionRequest.builder().transactionType(TransactionType.AUTHORIZE.name()).build();
        Transaction transaction = TransactionFactory.getTransaction(transactionRequest);

        Assertions.assertThat(transaction).isNotNull().isInstanceOf(AuthorizeTransaction.class);
    }
}
