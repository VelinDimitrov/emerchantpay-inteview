package com.example.emerchantpay.payment.factories;

import com.example.emerchantpay.payment.entities.*;
import com.example.emerchantpay.payment.pojos.UserCsvCreationDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserFactoryTest {
    private static final String USER_PASSWORD = "123456";
    private static final String USER_EMAIL = "test@mail.com";
    private static final String USER_NAME = "Test name";

    @Test
    public void getUser_getAdmin(){
        UserCsvCreationDto userCsvCreationDto = UserCsvCreationDto.builder()
                .userType(UserType.ADMIN.name())
                .password(USER_PASSWORD)
                .email(USER_EMAIL)
                .name(USER_NAME)
                .build();
        User user = UserFactory.getUser(userCsvCreationDto);
        Assertions.assertThat(user).isNotNull().isInstanceOf(AdminUser.class);
    }

    @Test
    public void getUser_getMerchant(){
        UserCsvCreationDto userCsvCreationDto = UserCsvCreationDto.builder()
                .userType(UserType.MERCHANT.name())
                .password(USER_PASSWORD)
                .email(USER_EMAIL)
                .name(USER_NAME)
                .status(MerchantStatus.ACTIVE.name())
                .build();
        User user = UserFactory.getUser(userCsvCreationDto);
        Assertions.assertThat(user).isNotNull().isInstanceOf(Merchant.class);
    }
}
