package com.example.emerchantpay.payment.utils;

import lombok.NoArgsConstructor;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

@NoArgsConstructor
public class ValidationUtils {
    private static Validator INSTANCE;

    public static Validator getValidator() {
        if (INSTANCE == null) {
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            INSTANCE = factory.getValidator();
        }
        return INSTANCE;
    }
}
