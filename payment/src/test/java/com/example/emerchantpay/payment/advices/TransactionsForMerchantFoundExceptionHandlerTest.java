package com.example.emerchantpay.payment.advices;

import com.example.emerchantpay.payment.pojos.ErrorResponse;
import com.example.emerchantpay.payment.exceptions.TransactionsForMerchantFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public class TransactionsForMerchantFoundExceptionHandlerTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");

    private final TransactionsForMerchantFoundExceptionHandler testHandler = new TransactionsForMerchantFoundExceptionHandler();
    @Test
    void responseBodyForTransactionsForMerchantFoundException() {
        ResponseEntity<ErrorResponse> exception = testHandler.exception(new TransactionsForMerchantFoundException(MERCHANT_ID));
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        String expectedErrorMessage = new ErrorResponse("Merchant with id: 3fa85f64-5717-4562-b3fc-2c963f66afa6 has transactions and cannot be deleted!").getErrorMessage();
        Assertions.assertThat(exception.getBody()).isNotNull().extracting(ErrorResponse::getErrorMessage).isEqualTo(expectedErrorMessage);
    }
}
