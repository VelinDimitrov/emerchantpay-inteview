package com.example.emerchantpay.payment.endpoints;

import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import com.example.emerchantpay.payment.services.interfaces.TransactionService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class TransactionEndpointTest {

    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa5");

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private TransactionEndpoint transactionEndpoint;

    @Test
    void getTransactionTest() {
        ResponseEntity<List<TransactionResponse>> response = transactionEndpoint.getTransactionsForMerchant(MERCHANT_ID,null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void createTransactionTest() {
        Mockito.when(transactionService.createTransaction(MERCHANT_ID, TransactionRequest.builder().build(),null)).thenReturn(TransactionResponse.builder().build());
        ResponseEntity<TransactionResponse> response = transactionEndpoint.createTransaction(MERCHANT_ID, TransactionRequest.builder().build(), null);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
}
