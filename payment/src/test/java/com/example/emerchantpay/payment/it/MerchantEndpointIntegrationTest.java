package com.example.emerchantpay.payment.it;

import com.example.emerchantpay.payment.endpoints.MerchantEndpoint;
import com.example.emerchantpay.payment.entities.MerchantStatus;
import com.example.emerchantpay.payment.entities.UserType;
import com.example.emerchantpay.payment.pojos.MerchantRequest;
import com.example.emerchantpay.payment.pojos.MerchantResponse;
import com.example.emerchantpay.payment.pojos.UserDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

@SpringBootTest
@ActiveProfiles("integartion")
public class MerchantEndpointIntegrationTest {
    private static final UUID FLYWAY_MERCHANT_ID = UUID.fromString("28cb74af-621a-4650-a5fe-0bee2470a592");
    @Autowired
    private MerchantEndpoint merchantEndpoint;

    @Test
    void testGetMerchantAddedFromFlyway(){
        UserDto userDto = UserDto.builder().id(FLYWAY_MERCHANT_ID).userType(UserType.MERCHANT.name()).build();
        ResponseEntity<MerchantResponse> response = merchantEndpoint.getMerchant(FLYWAY_MERCHANT_ID,userDto);
        Assertions.assertThat(response).isNotNull().extracting(ResponseEntity::getStatusCode).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testMerchantCreation(){
        MerchantRequest request = MerchantRequest.builder()
                .email(UUID.randomUUID()+ "@mail.com").name("it test user")
                .status(MerchantStatus.ACTIVE.name()).password("123456").build();
        ResponseEntity<MerchantResponse> response = merchantEndpoint.createMerchant(request);
        Assertions.assertThat(response).isNotNull().extracting(ResponseEntity::getStatusCode).isEqualTo(HttpStatus.CREATED);
        Assertions.assertThat(response.getHeaders()).containsKey("Location");
    }
}
