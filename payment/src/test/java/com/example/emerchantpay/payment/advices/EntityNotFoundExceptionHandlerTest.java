package com.example.emerchantpay.payment.advices;

import com.example.emerchantpay.payment.pojos.ErrorResponse;
import com.example.emerchantpay.payment.exceptions.MerchantNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public class EntityNotFoundExceptionHandlerTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    
    private final EntityNotFoundExceptionHandler testHandler = new EntityNotFoundExceptionHandler();
    @Test
    void responseBodyForEntityNotFoundHandler() {
        ResponseEntity<ErrorResponse> exception = testHandler.exception(new MerchantNotFoundException(MERCHANT_ID));
        Assertions.assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        String expectedErrorMessage = new ErrorResponse("Merchant not found for id: " + MERCHANT_ID).getErrorMessage();
        Assertions.assertThat(exception.getBody()).isNotNull().extracting(ErrorResponse::getErrorMessage).isEqualTo(expectedErrorMessage);
    }
}
