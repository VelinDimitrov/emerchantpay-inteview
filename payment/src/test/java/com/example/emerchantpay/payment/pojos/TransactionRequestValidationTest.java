package com.example.emerchantpay.payment.pojos;

import com.example.emerchantpay.payment.utils.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionRequestValidationTest {

    @Test
    public void whenAllInvalid_thenViolationsShouldBeReported() {
        TransactionRequest transactionRequest = TransactionRequest.builder().build();
        var violations = ValidationUtils.getValidator().validate(transactionRequest);

        assertThat(violations.size()).isEqualTo(3);
        assertThat(violations)
                .extracting(ConstraintViolation::getMessage)
                .contains( "Transaction type is mandatory", "Customer email is mandatory", "Customer phone is mandatory");
    }

    @Test
    public void whenValidValues_thenViolationsShouldNotBeReported() {
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.ONE).customerEmail("test@mail.com").customerPhone("432432432432").transactionType("CHARGE").build();
        var violations = ValidationUtils.getValidator().validate(transactionRequest);

        assertThat(violations.size()).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, -3, -5, -10, Integer.MIN_VALUE})
    public void whenAmountIsZeroOrLess_thenViolationsShouldBeReported(double amount) {
        TransactionRequest transactionRequest = TransactionRequest.builder().amount(BigDecimal.valueOf(amount)).customerEmail("test@mail.com").transactionType("CHARGE").customerPhone("432432432432").build();
        var violations = ValidationUtils.getValidator().validate(transactionRequest);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations)
                .extracting(ConstraintViolation::getMessage)
                .contains("Amount should be bigger than 0");
    }

}
