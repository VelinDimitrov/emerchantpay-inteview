package com.example.emerchantpay.payment.services;

import com.example.emerchantpay.payment.entities.Merchant;
import com.example.emerchantpay.payment.entities.MerchantStatus;
import com.example.emerchantpay.payment.entities.Transaction;
import com.example.emerchantpay.payment.entities.TransactionStatus;
import com.example.emerchantpay.payment.exceptions.NoActiveMerchantFoundException;
import com.example.emerchantpay.payment.pojos.TransactionRequest;
import com.example.emerchantpay.payment.pojos.TransactionResponse;
import com.example.emerchantpay.payment.entities.TransactionType;
import com.example.emerchantpay.payment.repositories.MerchantRepository;
import com.example.emerchantpay.payment.repositories.TransactionRepository;
import com.example.emerchantpay.payment.services.interfaces.MerchantService;
import com.example.emerchantpay.payment.services.interfaces.TransactionTypeProcessorService;
import com.example.emerchantpay.payment.services.interfaces.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
    private static final UUID MERCHANT_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    private static final UUID TRANSACTION_ID = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa5");
    private static final String CUSTOMER_EMAIL = "email@gmail.com";
    private static final String CUSTOMER_PHONE = "089364156456";
    private static final String MERCHANT_EMAIL = "merchant@gmail.com";
    private static final String MERCHANT_NAME = "Merchant123";

    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private MerchantRepository merchantRepository;
    @Mock
    private MerchantService merchantService;
    @Mock
    private TransactionTypeProcessorService transactionTypeProcessorService;
    @Mock
    private UserService userService;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    private Transaction transaction;
    private Merchant merchant;

    @BeforeEach
    public void setUp() {
        transaction = new Transaction();
        merchant = new Merchant();
    }

    @Test
    void getTransactionsForMerchantTest() {
        transaction.setStatus(TransactionStatus.APPROVED);
        transaction.setTransactionDate(LocalDateTime.MAX);
        transaction.setCustomerEmail(CUSTOMER_EMAIL);
        transaction.setCustomerPhone(CUSTOMER_PHONE);
        transaction.setId(TRANSACTION_ID);

        UUID secondTransactionId = UUID.randomUUID();
        Transaction secondTransaction = new Transaction(secondTransactionId, TransactionStatus.REFUNDED, TransactionType.AUTHORIZE, "test@mail.com", "65264879", LocalDateTime.MIN, null, null);
        Mockito.when(transactionRepository.findAllByMerchantIdOrderByTransactionDate(MERCHANT_ID)).thenReturn(Arrays.asList(transaction, secondTransaction));

        merchant.setStatus(MerchantStatus.ACTIVE);
        merchant.setEmail(MERCHANT_EMAIL);
        merchant.setName(MERCHANT_NAME);
        merchant.setId(MERCHANT_ID);

        Mockito.when(merchantRepository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));
        List<TransactionResponse> transactionsForMerchant = transactionService.getTransactionsForMerchant(MERCHANT_ID,null );
        Mockito.verify(transactionRepository).findAllByMerchantIdOrderByTransactionDate(MERCHANT_ID);

        TransactionResponse expectedFirstTransaction = TransactionResponse.builder()
                .transactionDate(LocalDateTime.MAX)
                .customerEmail(CUSTOMER_EMAIL)
                .customerPhone(CUSTOMER_PHONE)
                .id(TRANSACTION_ID)
                .status(TransactionStatus.APPROVED.name())
                .build();
        TransactionResponse expectedSecondTransaction = TransactionResponse.builder()
                .transactionDate(LocalDateTime.MIN)
                .customerEmail("test@mail.com")
                .customerPhone("65264879")
                .transactionType(TransactionType.AUTHORIZE.name())
                .id(secondTransactionId)
                .status(TransactionStatus.REFUNDED.name())
                .build();
        Assertions.assertThat(transactionsForMerchant).isEqualTo(Arrays.asList(expectedFirstTransaction, expectedSecondTransaction));
    }

    @Test
    void createTransactionTest() {
        Mockito.when(merchantRepository.findByIdAndStatus(MERCHANT_ID, MerchantStatus.ACTIVE)).thenReturn(new Merchant());

        transactionService.createTransaction(MERCHANT_ID, TransactionRequest.builder().transactionType("CHARGE").build(),null );
        Mockito.verify(transactionRepository).save(Mockito.any(Transaction.class));
        Mockito.verify(transactionTypeProcessorService).processTransaction(Mockito.any(UUID.class),Mockito.any(TransactionRequest.class),Mockito.any(Transaction.class));
    }

    @Test
    void createTransactionTest_throwExceptionNoActiveMerchantFound() {
        Mockito.when(merchantRepository.findByIdAndStatus(MERCHANT_ID, MerchantStatus.ACTIVE)).thenReturn(null);

        Assertions.assertThatThrownBy(() -> transactionService.createTransaction(MERCHANT_ID, TransactionRequest.builder().build(),null ))
                .isInstanceOf(NoActiveMerchantFoundException.class)
                .hasMessageContaining("No active Merchant found for id: 3fa85f64-5717-4562-b3fc-2c963f66afa6");
        Mockito.verify(transactionRepository, Mockito.never()).save(Mockito.any(Transaction.class));
    }
}
