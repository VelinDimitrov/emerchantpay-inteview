import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import MerchantsPage from "./components/MerchantsPage";
import MerchantForm from "./components/MerchantForm";
import TransactionsPage from "./components/TransactionsPage";
import LoginForm from "./components/LoginForm";
import { useUserContext } from "./components/context/UserContext";
import HomePage from "./components/HomePage";
import Navigation from "./components/Navigation";
import { ReactSession } from "react-client-session";

ReactSession.setStoreType("localStorage");

export const App = () => {
  const { isLoggedIn, isAdmin } = useUserContext();

  return (
    <BrowserRouter>
      <Navigation />
      <Routes>
        <Route exact path="/merchants/create" element={<MerchantForm />} />
        <Route exact path="/login" element={<LoginForm />} />
        {isLoggedIn && <Route exact path="/" element={<HomePage />} />}
        {isAdmin && (
          <Route exact path="/merchants" element={<MerchantsPage />} />
        )}
        <Route
          exact
          path="/merchants/:merchantId/edit"
          element={<MerchantForm />}
        />
        {isLoggedIn && (
          <Route
            exact
            path="/merchants/:merchantId/transactions"
            element={<TransactionsPage />}
          />
        )}
        <Route path="*" element={<Navigate to="/login" replace />} />
      </Routes>

      <Toaster
        toastOptions={{
          duration: 5000,
        }}
      />
    </BrowserRouter>
  );
};
