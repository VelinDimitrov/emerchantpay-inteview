import { get } from "./request";

const getTransactionsUrl = (merchantId) =>
  `http://localhost:8080/merchants/${merchantId}/transactions`;

export const getAllTransactions = (merchantId) =>
  get(getTransactionsUrl(merchantId));
