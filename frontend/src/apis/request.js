import axios from "axios";

axios.defaults.baseURL = "http://localhost:8080";

const getAuthToken = () => window.localStorage.getItem("auth_token");

export const setAuthToken = (token) =>
  window.localStorage.setItem("auth_token", token);

export const request = (method, url, data) => {
  let headers = {};
  const token = getAuthToken();
  if (token && token !== null && token !== "undefined" && token !== "null") {
    headers = { Authorization: `Bearer ${token}` };
  }
  return axios({ method, url, data, headers });
};

export const get = (url) => {
  return request("GET", url);
};

export const post = (url, data) => {
  return request("POST", url, data);
};

export const put = (url, data) => {
  return request("PUT", url, data);
};

export const deleteReq = (url) => {
  return request("DELETE", url);
};
