import { get, post, put, deleteReq } from "./request";

const merchantsUrl = "/merchants";

export const getAllMerchants = () => get(merchantsUrl);

export const getMerchant = (id) => get(`${merchantsUrl}/${id}`);

export const createMerchant = (merchantData) =>
  post(merchantsUrl, merchantData);

export const updateMerchant = (id, merchantData) =>
  put(`${merchantsUrl}/${id}`, merchantData);

export const deleteMerchant = (id) => deleteReq(`${merchantsUrl}/${id}`);
