import { post } from "./request";

const loginUrl = "/login";

export const login = (loginData) => post(loginUrl, loginData);
