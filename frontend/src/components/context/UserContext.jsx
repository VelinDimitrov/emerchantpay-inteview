import React, { useContext, useEffect, useState } from "react";
import { setAuthToken } from "../../apis/request";
import { ReactSession } from "react-client-session";

const UserContext = React.createContext();

export const UserProvider = ({ children }) => {
  let userDataFromStorage = ReactSession.get("userData");
  const [userData, setUserData] = useState(userDataFromStorage);

  useEffect(() => {
    ReactSession.set("userData", userData);
    setAuthToken(userData.token);
  }, [userData]);

  const clearContext = () => {
    ReactSession.set("userData", {});
    setUserData({});
  };
  return (
    <UserContext.Provider
      value={{
        userData,
        setUserData,
        clearContext,
        isLoggedIn: userData.token,
        isAdmin: userData.token && userData.userType === "ADMIN",
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUserContext = () => useContext(UserContext);
