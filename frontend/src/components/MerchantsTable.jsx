import Table from "react-bootstrap/Table";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import styled from "styled-components";
import { deleteMerchant } from "../apis/merchants";
import { useNavigate } from "react-router-dom";
import { useCallback } from "react";
import { toast } from "react-hot-toast";

const MerchantsTable = ({ merchants, refetch }) => {
  let navigate = useNavigate();

  const onDelete = useCallback(
    async (id) => {
      toast.promise(deleteMerchant(id), {
        loading: "Loading...",
        success: () => {
          refetch();
          return "Successfully Deleted!";
        },
        error: ({ response }) => {
          return response.data.errorMessage;
        },
      });
    },
    [refetch]
  );

  return (
    <MerchantTable striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Status</th>
          <th>Description</th>
          <th>Total transaction sum</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {merchants?.map((merchant, index) => (
          <tr key={merchant.id}>
            <td>{index + 1}</td>
            <td>{merchant.name}</td>
            <td>{merchant.email}</td>
            <td>
              {merchant.status === "ACTIVE" ? (
                <Badge bg="primary">Active</Badge>
              ) : (
                <Badge bg="secondary">Inactive</Badge>
              )}
            </td>
            <td>{merchant.description}</td>
            <td>{merchant.totalTransactionSum} $</td>
            <td>
              <Button
                variant="info"
                onClick={() =>
                  navigate(`/merchants/${merchant.id}/transactions`)
                }
              >
                Transactions
              </Button>{" "}
              <Button
                variant="warning"
                onClick={() => navigate(`/merchants/${merchant.id}/edit`)}
              >
                Edit
              </Button>{" "}
              <Button variant="danger" onClick={() => onDelete(merchant.id)}>
                Delete
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </MerchantTable>
  );
};

const MerchantTable = styled(Table)`
  width: 80%;
`;

export default MerchantsTable;
