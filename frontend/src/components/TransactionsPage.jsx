import { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { Button, Container } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { getAllTransactions } from "../apis/transactions";
import { TransactionsTable } from "./TransactionsTable";
import { useUserContext } from "./context/UserContext";

const TransactionsPage = ({ merchantId }) => {
  const { isLoggedIn, isAdmin, userData } = useUserContext();
  let navigate = useNavigate();
  const { merchantId: merchantIdFromUrl } = useParams();
  const [transactions, setTransactions] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchTransactions = useCallback(
    async (merchantId) => {
      if (merchantId && userData.token) {
        const resp = await getAllTransactions(merchantId);
        setLoading(false);
        setTransactions(resp.data);
      }
    },
    [setTransactions, userData.token]
  );

  useEffect(() => {
    if (merchantId) {
      fetchTransactions(merchantId);
    } else {
      fetchTransactions(merchantIdFromUrl);
    }
  }, [fetchTransactions, merchantId, merchantIdFromUrl]);
  return (
    <div>
      <TableContainer>
        <h2>Transactions</h2>
        <ButtonContainer>
          {isLoggedIn && isAdmin && (
            <Button
              onClick={() => navigate("/merchants")}
              variant="warning"
              className="me-4"
            >
              Back
            </Button>
          )}
        </ButtonContainer>
        {!loading && transactions.length > 0 && (
          <TransactionsTable transactions={transactions} />
        )}

        {!loading && transactions.length === 0 && (
          <h2>No transactions for merchant</h2>
        )}
      </TableContainer>
    </div>
  );
};

export default TransactionsPage;

const TableContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const ButtonContainer = styled(Container)`
  display: flex;
  width: 100%;
  flex-direction: row-reverse;
  margin-bottom: 1rem;
`;
