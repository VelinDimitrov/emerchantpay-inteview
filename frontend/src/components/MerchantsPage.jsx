import { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { getAllMerchants } from "../apis/merchants";
import MerchantsTable from "./MerchantsTable";
import { useUserContext } from "./context/UserContext";

const MerchantsPage = () => {
  const { isLoggedIn, isAdmin } = useUserContext();
  const [merchants, setMerchants] = useState(null);

  const fetchMerchants = useCallback(async () => {
    const resp = await getAllMerchants();
    setMerchants(resp.data);
  }, []);

  useEffect(() => {
    if (isLoggedIn && isAdmin) {
      fetchMerchants();
    }
  }, [fetchMerchants, isAdmin, isLoggedIn]);
  return (
    <div>
      <TableContainer>
        <h2>Merchants</h2>
        <MerchantsTable merchants={merchants} refetch={fetchMerchants} />
      </TableContainer>
    </div>
  );
};

export default MerchantsPage;

const TableContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
