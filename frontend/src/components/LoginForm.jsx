import Form from "react-bootstrap/Form";
import { Button, Container } from "react-bootstrap";
import styled from "styled-components";
import { useCallback, useState } from "react";
import { login } from "../apis/auth";
import { useUserContext } from "./context/UserContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-hot-toast";

const LoginForm = () => {
  let navigate = useNavigate();
  const { setUserData } = useUserContext();
  const [loginState, setLoginState] = useState({ email: "", password: "" });

  const handleOnChange = useCallback(
    (name, value) =>
      setLoginState((prevState) => ({ ...prevState, [name]: value })),
    [setLoginState]
  );

  const onLogin = useCallback(async () => {
    const response = await login(loginState);
    setUserData(response.data);
    navigate("/");
    toast.success("Successfully logged in!");
  }, [loginState, setUserData, navigate]);

  return (
    <StyledFormContainer>
      <StyledForm>
        <h2 className="text-center">Login</h2>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={loginState.email}
            onChange={(e) => handleOnChange("email", e.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={loginState.password}
            onChange={(e) => handleOnChange("password", e.target.value)}
          />
        </Form.Group>
        <Button variant="primary" onClick={onLogin}>
          Submit
        </Button>
      </StyledForm>
    </StyledFormContainer>
  );
};

export default LoginForm;

const StyledFormContainer = styled(Container)`
  display: flex;
  justify-content: center;
`;

const StyledForm = styled(Form)`
  width: 50%;
`;
