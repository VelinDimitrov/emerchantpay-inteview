import { useCallback, useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useParams, useNavigate } from "react-router-dom";
import styled from "styled-components";
import toast from "react-hot-toast";
import { createMerchant, getMerchant, updateMerchant } from "../apis/merchants";
import { useUserContext } from "./context/UserContext";

const validateForm = (formValues, isAdmin) => {
  const errList = [];
  if (!formValues.name) {
    errList.push("Name is required!");
  }
  if (!formValues.status) {
    errList.push("Status is required!");
  }
  if (!formValues.description) {
    errList.push("Description is required!");
  }
  if (!formValues.email) {
    errList.push("Email is required!");
  }
  if (!isAdmin) {
    if (!formValues.password) {
      errList.push("Password is required!");
    } else if (formValues.password && formValues.password.length < 6) {
      errList.push("Password should be minimum 6 chars!");
    }
  }

  return errList;
};

const MerchantForm = () => {
  const { isLoggedIn, isAdmin } = useUserContext();
  const { merchantId } = useParams();
  let navigate = useNavigate();
  const [formState, setFormState] = useState({ status: "ACTIVE" });

  const fetchMerchant = useCallback(async () => {
    if (merchantId) {
      const resp = await getMerchant(merchantId);
      setFormState({ ...resp.data });
    }
  }, [setFormState, merchantId]);

  useEffect(() => {
    fetchMerchant();
  }, [fetchMerchant]);

  const handleOnChange = useCallback(
    (name, value) =>
      setFormState((prevState) => ({ ...prevState, [name]: value })),
    [setFormState]
  );

  const onSubmit = useCallback(() => {
    const errors = validateForm(formState, isAdmin);
    if (errors.length > 0) {
      toast.error(errors.join("\n"));
      return;
    }

    if (merchantId) {
      toast.promise(updateMerchant(merchantId, { ...formState }), {
        loading: "Loading...",
        success: () => {
          navigate("/merchants");
          return "Successfully Completed!";
        },
        error: "Error while actioning!",
      });
      return;
    }

    toast.promise(createMerchant({ ...formState }), {
      loading: "Loading...",
      success: () => {
        navigate("/");
        return "Successfully Completed!";
      },
      error: "Error while actioning!",
    });
  }, [formState, navigate, merchantId, isAdmin]);

  return (
    <StyledFormContainer>
      <StyledForm>
        <h2 className="text-center">
          {merchantId ? "Modify" : "Register"} Merchant
        </h2>

        <Form.Group className="mb-3">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Name"
            onChange={(e) => handleOnChange("name", e.target.value)}
            value={formState.name ?? ""}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            onChange={(e) => handleOnChange("email", e.target.value)}
            value={formState.email ?? ""}
          />
        </Form.Group>
        {!isAdmin && (
          <Form.Group className="mb-3">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter password"
              onChange={(e) => handleOnChange("password", e.target.value)}
              value={formState.password ?? ""}
            />
          </Form.Group>
        )}

        <Form.Group className="mb-3">
          <Form.Label>Status</Form.Label>
          <Form.Select
            aria-label="Default select example"
            onChange={(e) => handleOnChange("status", e.target.value)}
            value={formState.status}
            disabled={!isLoggedIn}
          >
            <option value="ACTIVE">Active</option>
            <option value="INACTIVE">Inactive</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Enter Description"
            onChange={(e) => handleOnChange("description", e.target.value)}
            value={formState.description ?? ""}
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Button
            onClick={() => navigate("/merchants")}
            variant="warning"
            className="me-2"
          >
            Back
          </Button>
          <Button
            onClick={(e) => {
              e.preventDefault();
              onSubmit();
            }}
          >
            {merchantId ? "Modify" : "Register"}
          </Button>
        </Form.Group>
      </StyledForm>
    </StyledFormContainer>
  );
};

export default MerchantForm;

const StyledFormContainer = styled(Container)`
  display: flex;
  justify-content: center;
`;

const StyledForm = styled(Form)`
  width: 50%;
`;
