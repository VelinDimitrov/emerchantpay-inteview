import { useUserContext } from "./context/UserContext";
import MerchantsPage from "./MerchantsPage";
import TransactionsPage from "./TransactionsPage";

const HomePage = () => {
  const { isLoggedIn, isAdmin, userData } = useUserContext();

  const getTransactionPage = () => {
    if (isLoggedIn && !isAdmin) {
      return <TransactionsPage merchantId={userData.id} />;
    }
    return <TransactionsPage />;
  };
  return isAdmin ? <MerchantsPage /> : getTransactionPage();
};

export default HomePage;
