import Nav from "react-bootstrap/Nav";
import { useUserContext } from "./context/UserContext";
import { Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { toast } from "react-hot-toast";

const Navigation = () => {
  let navigate = useNavigate();
  const { isLoggedIn, isAdmin, userData, clearContext } = useUserContext();
  return (
    <Nav variant="tabs" className="navbar bg-light">
      {!isLoggedIn && (
        <Nav.Item as={Link} to="/login" className="nav-link">
          Login
        </Nav.Item>
      )}
      {!isLoggedIn && (
        <Nav.Item as={Link} to="/merchants/create" className="nav-link">
          Register as merchant
        </Nav.Item>
      )}
      {isLoggedIn && isAdmin && (
        <Nav.Item as={Link} to="/merchants" className="nav-link">
          Merchants
        </Nav.Item>
      )}
      {isLoggedIn && !isAdmin && (
        <Nav.Item as={Link} to="/" className="nav-link">
          My Transactions
        </Nav.Item>
      )}
      <RightSideButtons>
        {isLoggedIn &&
          `Welcome ${userData?.userType.slice(0, 1)}${userData?.userType
            .slice(1)
            .toLowerCase()} ${userData?.name}`}
        {isLoggedIn && (
          <Button
            onClick={() => {
              clearContext();
              navigate("/");
              toast.success("Successfully logged out!");
            }}
          >
            Logout
          </Button>
        )}
      </RightSideButtons>
    </Nav>
  );
};

export default Navigation;

const RightSideButtons = styled.div`
  display: flex;
  align-items: center;
  gap: 1rem;
  margin-left: auto;
`;
