import Table from "react-bootstrap/Table";
import Badge from "react-bootstrap/Badge";

const getStatusObj = (status) => {
  if (status === "APPROVED") {
    return { label: "Approved", bg: "success" };
  } else if (status === "REVERSED") {
    return { label: "Reversed", bg: "warning" };
  } else if (status === "REFUNDED") {
    return { label: "Refunded", bg: "info" };
  }

  return { label: "Error", bg: "danger" };
};

export const TransactionsTable = ({ transactions }) => (
  <Table striped bordered hover>
    <thead>
      <tr>
        <th>#</th>
        <th>Amount</th>
        <th>Transaction type</th>
        <th>Status</th>
        <th>Transaction Date</th>
        <th>Customer Email</th>
        <th>Customer Phone</th>
      </tr>
    </thead>
    <tbody>
      {transactions?.map((transaction, index) => {
        const badgeStatusObj = getStatusObj(transaction.status);
        return (
          <tr key={transaction.id}>
            <td>{index + 1}</td>
            <td>{transaction.amount && `${transaction.amount} $`}</td>
            <td>{`${transaction.transactionType.slice(
              0,
              1
            )}${transaction.transactionType.slice(1).toLowerCase()}`}</td>
            <td>
              <Badge bg={badgeStatusObj.bg}>{badgeStatusObj.label}</Badge>
            </td>
            <td>{new Date(transaction.transactionDate).toLocaleString()}</td>
            <td>{transaction.customerEmail}</td>
            <td>{transaction.customerPhone}</td>
          </tr>
        );
      })}
    </tbody>
  </Table>
);
